# Cloud tools

On this address [https://gitlab.ics.muni.cz/cloud/cloud-tools](https://gitlab.ics.muni.cz/cloud/cloud-tools) you can find a docker container with all modules required for cloud management, if you are interested in managing your cloud platform via CLI. If so, you can check our guide how to use CLI cloud interface [here](https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/).