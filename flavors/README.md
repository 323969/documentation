# Flavors and Images

In this guide you can find info about all flavors available in Metacentrum Cloud.

<sup>Flavor name</sup> | <sup>CPU</sup> | <sup>RAM (in GB)</sup> | <sup>HPC</sup> | <sup>SSD</sup> | <sup>Disc throughput (in MB per second)</sup> | <sup>IOPS</sup> | <sup>Net average througput (in MB per second)</sup> | <sup>GPU</sup>
-------| -------| -------| -------| -------| -------| -------| -------| -------
<sup>elixir.hda1</sup> | <sup>30</sup> | <sup>724</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>elixir.hda1-10core-240ram</sup> | <sup>10</sup> | <sup>240</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.16core-128ram</sup> | <sup>16</sup> | <sup>128</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.16core-32ram</sup> | <sup>16</sup> | <sup>32</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.16core-64ram-ssd-ephem</sup> | <sup>16</sup> | <sup>64</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.18core-48ram</sup> | <sup>18</sup> | <sup>48</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.19core-176ram-nvidia-1080-glados</sup> | <sup>19</sup> | <sup>176</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.19core-176ram-nvidia-2080-glados</sup> | <sup>19</sup> | <sup>176</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.24core-256ram-ssd-ephem</sup> | <sup>24</sup> | <sup>256</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.24core-96ram-ssd-ephem</sup> | <sup>24</sup> | <sup>96</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.30core-128ram-ssd-ephem-500</sup> | <sup>30</sup> | <sup>128</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.30core-256ram</sup> | <sup>30</sup> | <sup>256</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.30core-64ram</sup> | <sup>30</sup> | <sup>64</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.32core-256ram-nvidia-t4-single-gpu</sup> | <sup>32</sup> | <sup>240</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.38core-372ram-nvidia-1080-glados</sup> | <sup>38</sup> | <sup>352</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.38core-372ram-nvidia-2080-glados</sup> | <sup>38</sup> | <sup>352</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.38core-372ram-ssd-ephem</sup> | <sup>38</sup> | <sup>372</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.40core-372ram-nvidia-1080-glados</sup> | <sup>40</sup> | <sup>352</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.40core-372ram-nvidia-2080-glados</sup> | <sup>40</sup> | <sup>352</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.40core-372ram-nvidia-titan-glados</sup> | <sup>40</sup> | <sup>352</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.4core-16ram-ssd-ephem</sup> | <sup>4</sup> | <sup>16</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.4core-16ram-ssd-ephem-500</sup> | <sup>4</sup> | <sup>16</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.64core-512ram-nvidia-t4</sup> | <sup>64</sup> | <sup>480</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.8core-16ram</sup> | <sup>8</sup> | <sup>16</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.8core-32ram-ssd-ephem</sup> | <sup>8</sup> | <sup>32</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>hpc.8core-32ram-ssd-rcx-ephem</sup> | <sup>8</sup> | <sup>32</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.8core-64ram</sup> | <sup>8</sup> | <sup>64</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.8core-64ram-nvidia-1080-glados</sup> | <sup>8</sup> | <sup>64</sup> | <sup>Yes </sup>| <sup>Yes</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.hdh</sup> | <sup>32</sup> | <sup>480</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.hdh-8cpu-120ram</sup> | <sup>8</sup> | <sup>120</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.hdh-ephem</sup> | <sup>32</sup> | <sup>480</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.ics-gladosag-full</sup> | <sup>38</sup> | <sup>372</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>hpc.large</sup> | <sup>16</sup> | <sup>64</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.medium</sup> | <sup>8</sup> | <sup>32</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.nvidia-2080-hdg-16cpu-236ram-ephem</sup> | <sup>16</sup> | <sup>236</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.nvidia-2080-hdg-ephem</sup> | <sup>32</sup> | <sup>448</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.nvidia-2080-hdg-half-ephem</sup> | <sup>16</sup> | <sup>238</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Yes</sup>
<sup>hpc.small</sup> | <sup>4</sup> | <sup>16</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.xlarge</sup> | <sup>24</sup> | <sup>96</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>524.288</sup> | <sup>2000</sup> | <sup>2000.0</sup> | <sup>No</sup>
<sup>hpc.xlarge-memory</sup> | <sup>24</sup> | <sup>256</sup> | <sup>Yes </sup>| <sup>No</sup>          | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>Unlimited</sup> | <sup>No</sup>
<sup>standard.12core-24ram</sup> | <sup>12</sup> | <sup>24</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>625.0</sup> | <sup>No</sup>
<sup>standard.16core-32ram</sup> | <sup>16</sup> | <sup>32</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>625.0</sup> | <sup>No</sup>
<sup>standard.20core-128ram</sup> | <sup>20</sup> | <sup>128</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.20core-160ram</sup> | <sup>20</sup> | <sup>160</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>standard.20core-256ram</sup> | <sup>20</sup> | <sup>256</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>1250.0</sup> | <sup>No</sup>
<sup>standard.2core-16ram</sup> | <sup>2</sup> | <sup>16</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.large</sup> | <sup>4</sup> | <sup>8</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.medium</sup> | <sup>2</sup> | <sup>4</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.memory</sup> | <sup>2</sup> | <sup>32</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.one-to-many</sup> | <sup>20</sup> | <sup>64</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.small</sup> | <sup>1</sup> | <sup>2</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.tiny</sup> | <sup>1</sup> | <sup>1</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.xlarge</sup> | <sup>4</sup> | <sup>16</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.xlarge-cpu</sup> | <sup>8</sup> | <sup>16</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.xxlarge</sup> | <sup>8</sup> | <sup>32</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
<sup>standard.xxxlarge</sup> | <sup>8</sup> | <sup>64</sup> | <sup>No </sup>| <sup>No</sup>          | <sup>262.144</sup> | <sup>2000</sup> | <sup>250.0</sup> | <sup>No</sup>
