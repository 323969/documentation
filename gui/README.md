# Advanced Features

The following guide will introduce you to advanced features available in MetaCentrum Cloud.
For basic instructions on how to start a virtual machine instance, see [Quick Start](/quick-start/README.md).


## Orchestration

The OpenStack orchestration service can be used to deploy and manage complex virtual topologies as single entities,
including basic auto-scaling and self-healing.

**This feature is provided as is and configuration is entirely the responsibility of the user.**

For details, refer to [the official documentation](https://docs.openstack.org/heat-dashboard/train/user/index.html).

## Image upload

We don't support uploading own images by default. MetaCentrum Cloud images are optimized for running in the cloud and we recommend users
to customize them instead of building own images from scratch. If you need upload custom image, please contact user support for appropriate permissions.

Instructions for uploading custom image:

1. Upload only images in RAW format (not qcow2, vmdk, etc.).

2. Upload is supported only through OpenStack [CLI](https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/) with Application Credentials.

3. Each image needs to contain metadata:
```
hw_scsi_model=virtio-scsi
hw_disk_bus=scsi
hw_rng_model=virtio
hw_qemu_guest_agent=yes
os_require_quiesce=yes
```
Following needs to be setup correctly (consult official [documentation](https://docs.openstack.org/glance/train/admin/useful-image-properties.html#image-property-keys-and-values))
or instances won't start:
```
os_type=linux # example
os_distro=ubuntu # example
```

4. Images should contain cloud-init, qemu-guest-agent and grow-part tools

5. OpenStack will resize instance after start. Image shouldn't contain any empty partitions or free space

For mor detailed explanation about CLI work with images, please refer to [https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/image.html](https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/image.html).



## Image visibility
In OpenStack there are 4 possible visibilities of particular image:  **public, private, shared, community**.

### 1. Public image

  **Public image** is an image visible to everyone and everyone can access it.

### 2. Private image

  **Private image** is an image visible to only to owner of that image. This is default setting for all newly created images.

### 3. Shared image

  **Shared image** is an image visible to only to owner and possibly certain groups that owner specified. How to share an image between project, please read following [tutorial](/gui/#image-sharing-between-projects) below.

### 4. Community image
  **Community image** is an image that is accesible to everyone, however it is not visible in dashboard. These images can be listed in CLI via command:

   ```openstack image list --community```.

  This is especially beneficial in case of great number of users who should get access to this image or if you own image that is old but some users might still require that image. In that case you can make set old image and **Community image** and set new one as default.

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(255,0,0,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>WARNING:</strong><br/><br/>
    To create or upload this image you must have an <b>image_uploader</b> right.
  </div>

  Creating a new **Community image** can look like this:

  ```openstack image create --file test-cirros.raw --property hw_scsi_model=virtio-scsi --property hw_disk_bus=scsi --property hw_rng_model=virtio --property hw_qemu_guest_agent=yes --property os_require_quiesce=yes --property os_type=linux --community test-cirros```


For more detailed explanation about these properties, go to the following link: [https://wiki.openstack.org/wiki/Glance-v2-community-image-visibility-design](https://wiki.openstack.org/wiki/Glance-v2-community-image-visibility-design).

## Image sharing between projects
Image sharing allows you to share your image between different projects and then it is possible to launch instances from that image in those projects with other collaborators etc. As mentioned in section about CLI, you will need to use your OpenStack credentials from ```openrc``` or ```cloud.yaml``` file.

Then to share an image you need to know it's ID, which you can find with command:
```
openstack image show <name_of_image>
```
where ```name_of_image``` is name of image you want to share.


After that you will also have to know ID of project you want to share your image with. If you do not know ID of that project you can use following command, which can help you find it:
```
openstack project list | grep <name_of_other_project>
```
where ```<name_of_project>``` is name of other project. It's ID will show up in first column.

Now all with necessary IDs you can now share your image. First you need to set an attribute of image to `shared` by following command:
```
openstack image set --shared <image_ID>
```
And now you can share it with your project by typing this command:
```
openstack image add project <image_ID> <ID_of_other_project>
```
where ```ID_of_other_project``` is ID of project you want to share image with.

Now you can check if user of other project accepted your image by command:
```
openstack image member list <image_ID>
```
If the other user did not accepted your image yet, status column will contain value: ```pending```.

**Accepting shared image**

To accept shared image you need to know ```<image_ID>``` of image that other person wants to share with you. To accept shared image to your project
you need to use following command:
```
openstack image set --accept <image_ID>
```
You can then verify that by listing your images:
```
openstack image list | grep <image_ID>
```
**Unshare shared image**

As owner of the shared image, you can check all projects that have access to the shared image by following command:
```
openstack image member list <image_ID>
```
When you find ```<ID_project_to_unshare>``` of project, you can cancel access of that project to shared image by command:
```
openstack image remove project <image ID> <ID_project_to_unshare>
```

## Add SWAP file to instance

By default VMs after creation do not have SWAP partition. If you need to add a SWAP file to your system you can download and run [script](https://gitlab.ics.muni.cz/cloud/cloud-tools/-/blob/master/swap/swap.sh) that create SWAP file on your VM.

## Local SSDs

Default MetaCentrum Cloud storage is implemented via CEPH storage cluster deployed on top of HDDs. This configuration should be sufficient for most cases.
For instances, that requires high throughput and IOPS, it is possible to utilize hypervizor local SSDs. Requirements for instances on hypervizor local SSD:
* instances can be deployed only via API (CLI, Ansible, Terraform ...), instances deployed via web gui (Horizon) will always use CEPH for it's storage
* supported only by flavors with ssd-ephem suffix (e.g. hpc.4core-16ram-ssd-ephem)
* instances can be rebooted without prior notice or you can be required to delete them
* you can request them, when asking for new project, or for existing project on cloud@metacentrum.cz

## Affinity policy

Affinity policy is a tool users can use to deploy nodes of cluster on same physical machine or if they should be spread among other physical machines. This can be beneficial if you need fast communication between nodes or you need them to be spreaded due to load-balancing or high-availability etc. For more info please refer to [https://docs.openstack.org/senlin/train/scenarios/affinity.html](https://docs.openstack.org/senlin/train/scenarios/affinity.html).

## LBaaS - OpenStack Octavia

Load Balancer is a tool used for distributing a set of tasks over particular set of resources. Its main goal is to find an optimal use of resources and make processing of particular tasks more efficient.

In following example you can see how basic HTTP server is deployed via CLI.

**Requirements**:

- 2 instances connected to same internal subnet and configured with HTTP application on TCP port 80


```
openstack loadbalancer create --name my_lb --vip-subnet-id <external_subnet_ID>
```
where **<external_subnet_ID>** is an ID of external shared subnet created by cloud admins reachable from Internet.

You can check newly created Load Balancer by running following command:

```
openstack loadbalancer show my_lb
```
Now you must create listener on port 80 to enable incoming traffic by following command:

```
openstack loadbalancer listener create --name listener_http --protocol HTTP --protocol-port 80 my_lb
```

Now you must add a pool on created listener to setup configuration for Load Balancer. You can do it by following command:

```
openstack loadbalancer pool create --name pool_http --lb-algorithm ROUND_ROBIN --listener listener_http --protocol HTTP
```
Here you created pool using Round Robin algorithm for load balancing.

And now you must configure both nodes to join to Load Balancer:

```
openstack loadbalancer member create --subnet-id <internal_subnet_ID> --address 192.168.50.15 --protocol-port 80 pool_http
openstack loadbalancer member create --subnet-id <internal_subnet_ID> --address 192.168.50.16 --protocol-port 80 pool_http
```
where **<internal_subnet_ID>** is an ID of internal subnet used by your instances and **--address** specifies an adress of concrete instance.

For more info, please refer to [https://docs.openstack.org/octavia/train/user/guides/basic-cookbook.html#basic-lb-with-hm-and-fip](https://docs.openstack.org/octavia/train/user/guides/basic-cookbook.html#basic-lb-with-hm-and-fip).

LBaaS (Load Balancer as a service) provides user with load balancing service, that can be fully managed via OpenStack API (some basic tasks are supported by GUI). Core benefits:
* creation and management of load balancer resources can be easily automatized via API, or existing tools like Ansible or Terraform
* applications can be easily scaled by starting up more OpenStack instances and registering them into the load balancer
* public IPv4 addresses saving - you can deploy one load balancer with one public IP and serve multiple services on multiple pools of instances by TCP/UDP port or L7 policies

**This feature is provided as is and configuration is entirely the responsibility of the user.**

Official documentation for LBaaS (Octavia) service - https://docs.openstack.org/octavia/latest/user/index.html

##Cloud orchestration tools

### Terraform

Terraform is the best orchestration tool for creating and managing cloud infrastructure. It is capable of greatly simplifying cloud operations. It gives you an option if something goes goes wrong you can easily rebuild your cloud infrastructure.

It manages resources like virtual machines,DNS records etc..

It is managed through configuration templates containing info about its tasks and resources. They are saved as *.tf files. If configuration changes, Terraform is to able to detect it and create additional operations in order to apply those changes.

Here is an example how this configuration file can look like:

```
variable "image" {
default = "Debian 10"
}

variable "flavor" {
default = "standard.small"
}

variable "ssh_key_file" {
default = "~/.ssh/id_rsa"
}
```

 You can use OpenStack Provider which is tool for managing resources OpenStack supports via Terraform. Terraform has an advantage over Heat because it can be used als in other architectures, not only in OpenStack


For more detail please refer to [https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs) and [https://www.terraform.io/intro/index.html](https://www.terraform.io/intro/index.html).


### Heat
Heat is another orchestration tool used for managing cloud resources. This one is OpenStack exclusive so you can't use it anywhere else. Just like Terraform it is capable of simplifying orchestration operations in your cloud infrastructure.

It also uses configuration templates for specification of information about resources and tasks. You can manage resources like servers, floating ips, volumes, security groups etc. via Heat.

Here is an example of Heat configuration template in form of *.yaml file:


```
heat_template_version: 2021-04-06

description: Test template

resources:
  my_instance:
    type: OS::Nova::Server
    properties:
      key_name: id_rsa
      image: Debian10_image
      flavor: standard.small
```

You can find more information here [https://wiki.openstack.org/wiki/Heat](https://wiki.openstack.org/wiki/Heat).

##Object storage management

OpenStack supports object storage based on [OpenStack Swift](https://docs.openstack.org/swift/latest/api/object_api_v1_overview.html). Creation of object storage container (database) is done by clicking on `+Container` on [Object storage containers page](https://dashboard.cloud.muni.cz/project/containers).

Every object typically contains data along with metadata and unique global identifier to access it. OpenStack allows you to upload your files via HTTPs protocol. There are two ways managing created object storage container:

1. Use OpenStack component [Swift](https://docs.openstack.org/swift/train/admin/index.html)

2. Use [S3 API](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html)

In both cases you will need application credentials to be able to manage your data.


### Swift credentials

The easiest way to generate **Swift** storage credentials is through [MetaCentrum cloud dashboard](https://dashboard.cloud.muni.cz). You can generate application credentials as described [here](/cli/#getting-credentials). You must have role **heat_stack_owner**.

### S3 credentials

If you want to use **S3 API** you will need to generate ec2 credentials for access. Note that to generate ec2 credentials you will also need credentials containing role of **heat_stack_owner**. Once you sourced your credentials for CLI you can generate ec2 credentials by following command:

```
$ openstack ec2 credentials create          
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field      | Value                                                                                                                                                                    |
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| access     | 896**************************651                                                                                                                                         |
| project_id | f0c**************************508                                                                                                                                         |
| secret     | 336**************************49c                                                                                                                                         |
...
| user_id    | e65***********************************************************6a                                                                                                         |
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

```
Then you will obtain S3 credentials. There is also a possibility that you can encouter following problem while working with S3:

Then you may use one of the s3 clients (minio client mc, s3cmd, ...)
Running minio client against created object storage container is very easy:

```
$ MC config host add swift-s3 https://object-store.cloud.muni.cz  896**************************651 336**************************49c --api S3v2
Added `swift-s3` successfully.

$ MC ls swift-s3
[2021-04-19 15:13:45 CEST]     0B freznicek-test/
```
s3cmd client requires configuration file which looks like:
In this case please open your file with credentials which will look like this:
```
default]
access_key = 896**************************651
secret_key = 336**************************49c
host_base = object-store.cloud.muni.cz
host_bucket = object-store.cloud.muni.cz
use_https = True
```

For more info please refer to [https://docs.openstack.org/swift/latest/s3_compat.html](https://docs.openstack.org/swift/latest/s3_compat.html) and [https://docs.openstack.org/train/config-reference/object-storage/configure-s3.html](https://docs.openstack.org/train/config-reference/object-storage/configure-s3.html).
