# Networking

For the networking in Cloud2 metacentrum we need to distinguish following scenarios
* personal project
* group project.


   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(255,0,0,0.3);margin:20px 0;padding:10px 20px;font-size:15px;display: -webkit-inline-box;">
     <strong>WARNING:</strong><br/><br/>
     Please read the following rules:

   1. If you are using a <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/register/#personal-project">PERSONAL</a>  project you have to use the `78-128-250-pers-proj-net` network to make your instance accesible from external network (e.g. Internet). Use `public-cesnet-78-128-250-PERSONAL` for FIP allocation, FIPs from this pool will be periodically released.
   2. If you are using a <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/register/#group-project">GROUP</a>  project you may choose from the `public-cesnet-78-128-251-GROUP`, `public-muni-147-251-124-GROUP` or any other <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/register/#group-project">GROUP</a>  network for FIP allocation to make your instance accesible from external network (e.g. Internet).
   3. Violation of the network usage may lead into resource removal and reducing of the quotas assigned.
   </div>


## Networking in the personal vs. group projects

### Personal Project networking

Is currently limited to the common internal network. The network in which you should start your machine is called `78-128-250-pers-proj-net` and is selected by default when using dashboard to start a machine (if you do not have another network created). The floating IP adresses you need to access a virtual machine is `public-cesnet-78-128-250-PERSONAL`. Any other allocated floatin IP address and `external gateway` will be deleted. You cannot use router with the personal project and any previously created routers will be deleted.

### Group project

In group project situation is rather different. You cannot use the same approach as personal project (resources allocated in previously mentioned networks will be periodically released). For FIP you need to allocate from pools with `-GROUP` suffix (namely `public-cesnet-78-128-251-GROUP`, `public-muni-147-251-21-GROUP` or `public-muni-147-251-124-GROUP`).

 <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     If you use MUNI account, you can use private-muni-10-16-116 and log into the network via MUNI VPN or you can set up Proxy networking, which is described
     <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/network/#proxy-networking">here</a>
</div>


#### Virtual Networks

MetaCentrum Cloud offers software-defined networking as one of its services. Users have the ability to create their own
networks and subnets, connect them with routers, and set up tiered network topologies.

Prerequisites:
* Basic understanding of routing
* Basic understanding of TCP/IP

For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/create-networks.html).


#### Network creation

For group project you need to create internal network first, you may use autoallocated pool for subnet autocreation.
Navigate yourself towards **Network &gt; Networks** in the left menu and click on the **Create Network** on the right side of the window. This will start an interactive dialog for network creation.
![](/network/images/1.png)
![](/network/images/2.png)
Inside the interactive dialog:
1. Type in the network name
![](/network/images/3.png)
2. Move to the **Subnet** section either by clicking next or by clicking on the **Subnet** tab. You may choose to enter network range manually (recommended for advanced users in order to not interfere with the public IP address ranges), or select **Allocate Network Address from a pool**. In the **Address pool** section select a `private-192-168`. Select Network mask which suits your needs (`27` as default can hold up to 29 machines, use IP calculator if you are not sure).
![](/network/images/4.png)
3. For the last tab **Subnet Details** just check that a DNS is present and DHCP box is checked, alternatively you can create the allocation pool or specify static routes in here (for advanced users).
![](/network/images/5.png)


<div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
  <strong>Notice:</strong><br/>
  If you want to use CLI to create network, please go <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/#create-network">here</a>.
</div>


#### Proxy networking
In your OpenStack instances you can you private or public networks. If you use private network and you need to access to the internet for updates etc.,
you can visit following [link](/faq/#issues-with-proxy-in-private-networks), where it is explained, how to set up Proxy connection.


#### Setup Router gateway (Required for Group projects)
Completing [Create Virtual Machine Instance](/quick-start/#create-virtual-machine-instance) created instance connected
to software defined network represented by internal network, subnet and router. Router has by default gateway address
from External Network chosen by cloud administrators. You can change it to any External Network with [GROUP](/register/#group-project) suffix, that
is visible to you (e.g. **public-muni-147-251-124-GROUP** or **public-cesnet-78-128-251-GROUP**). Usage of External Networks
with suffix PERSONAL (e.g. **public-cesnet-78-128-250-PERSONAL**) is discouraged. IP addresses from
PERSONAL segments will be automatically released from Group projects.
For changing gateway IP address follow these steps:

1. In **Network &gt; Routers**, click the **Set Gateway** button next to router.
If router exists with another settings, then use button Clear Gateway, confirm Clear Gateway.
If router isn't set then use button Create router and choose network.

2. From list of External Network choose **public-cesnet-78-128-251-GROUP**, **public-muni-147-251-124-GROUP** or any other [GROUP](/register/#group-project)  network you see.

 ![](/network/images/network_routers-group.png)

Router is setup with persistent gateway.

#### Router creation

Navigate yourself towards **Network &gt; Routers** in the left menu and click on the **Create Router** on the right side of the window.
In the interactive dialog:
1. Enter router name and select external gateway with the `-GROUP` suffix.
![](/network/images/r1.png)

Now you need to attach your internal network to the router.
1. Click on the router you just created.
2. Move to the **Interfaces** tab and click on the **Add interface**.
![](/network/images/r2.png)
3. Select a previously created subnet and submit.
![](/network/images/r3.png)



  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    If you want to use CLI to manage routers, please go <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/#router-management">here</a>.
  </div>


<div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
  <strong>Notice:</strong><br/>
  Routers can also be used to route traffic between internal networks. This is an advanced topic not covered in this guide.
</div>


#### Associate Floating IP


   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(255,0,0,0.3);margin:20px 0;padding:10px 20px;font-size:15px;display: -webkit-inline-box;">
     <strong>WARNING:</strong><br/><br/>
     There is a limited number of Floating IP adresses. So please before you ask for more Floating IP address, visit and read <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/faq/#how-many-floating-ips-does-my-group-project-need">https://cloud.gitlab-pages.ics.muni.cz/documentation/faq/#how-many-floating-ips-does-my-group-project-need</a>.
   </div>




To make an instance accessible from external networks (e.g., The Internet), a so-called Floating IP Address has to be
associated with it.

1. In **Project &gt; Network &gt; Floating IPs**, select **Allocate IP to Project**. Pick an IP pool from which to allocate
   the address. Click on **Allocate IP**.

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     In case of group projects when picking an IP pool from which to allocate a floating IP address, please, keep in mind that you have to allocate
     an address in the pool connected to your virtual router.
   </div>

   <div style="border-width:0;border-left:5px solid #ff0000;background-color:rgba(255, 0, 0, 0.2);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Warning:</strong><br/>
     Group projects can persistently allocate IPs only from External Network with GROUP suffix (e.g. public-muni-147-251-124-GROUP or public-cesnet-78-128-251-GROUP).
     IPs from External Networks with suffix PERSONAL (e.g. public-cesnet-78-128-250-PERSONAL) will be released automatically.
   </div>

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     Please, keep an eye on the number of allocated IPs in <strong>Project &gt; Network &gt; Floating IPs</strong>. IPs
     remain allocated to you until you explicitly release them in this tab. Detaching an IP from an instance is not sufficient
     and the IP in question will remain allocated to you and consume your Floating IP quota.
   </div>

1. In **Project &gt; Compute &gt; Instances**, select **Associate Floating IP** from the **Actions** drop-down menu for the
   given instance.

2. Select IP address and click on **Associate**.

  ![](/quick-start/images/network_floating_ip.png)

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    If you want to use CLI to manage FIP, please go <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/#floating-ip-address-management">here</a>.
  </div>


## Change external network in GUI

Following chapter covers the problem of changing the external network via GUI or CLI.

### Existing Floating IP release

First you need to release existing Floating IPs from your instances - go to **Project &gt; Compute &gt;  Instances**. Click on the menu **Actions** on the instance you whish to change and **Disassociate Floating IP** and specify that you wish to **Release Floating IP** WARN: After this action your project will no longer be able to use the floating IP address you released. Confirm that you wish to disassociate the floating IP by clicking on the **Disassociate** button. When you are done with all instances connected to your router you may continue with the next step.
![](/network/images/instance1.png)

### Clear Gateway

Now, you should navigate yourself to the **Project &gt; Network &gt; Routers**. Click on the action **Clear Gateway** of your router. This action will disassociate the external network from your router, so your machines will not longer be able to access Internet. If you get an error go back to step 1 and **Disassociate your Floating IPs**.
![](/network/images/clear-router1.png)

### Set Gateway

1. Now, you can set your gateway by clicking **Set Gateway**.
![](/network/images/set-router1.png)

2. Choose network you desire to use (e.g. **public-cesnet-78-128-251**) and confirm.
![](/network/images/set-router2.png)

### Allocate new Floating IP(s)

<div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
  <strong>Warning:</strong><br/>
  New floating IP address for router must be from same network pool which was selected as new gateway.
</div>


1. Go to **Project &gt; Network &gt; Floating IPs** and click on the **Allocate IP to Project** button. Select **Pool** with the same value as the network you chose in the previous step and confirm it by clicking **Allocate IP**
![](/network/images/allocate-fip.png)

2. Now click on the **Associate** button next to the Floating IP you just created. Select **Port to be associated** with desired instance. Confirm with the **Associate** button. Repeat this section for all your machines requiring a Floating IP.
![](/network/images/associate-fip.png)
