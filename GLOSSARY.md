## Virtual Machine Instance
Server-like resource in OpenStack

## Security Group
Openstack firewall-like resource

## Project
Resource ownership unit in OpenStack

## Floating IP Address
Public/external IP resource in OpenStack

## Image
[OpenStack operating system image resource](https://docs.openstack.org/image-guide/introduction.html)

## Volume
[Storage allocation resource in OpenStack](https://docs.openstack.org/mitaka/user-guide/dashboard_manage_volumes.html)

## SSH
[Secure Shell](https://en.wikipedia.org/wiki/SSH_(Secure_Shell))

## SSH Key Pair
[Set of keys for asymmetric cryptography, used for secure remote access](https://en.wikipedia.org/wiki/Public-key_cryptography)

## User Support
[cloud@metacentrum.cz](mailto:cloud@metacentrum.cz)

## MUNI Identity Management
[idm@ics.muni.cz](mailto:idm@ics.muni.cz)

## VO
Virtual Organization, unit of organizational hierarchy
